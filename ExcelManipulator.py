'''
Created on 01 feb 2018

@author: GIULI370

@note: Tested on WinPython-64bit-3.5.2.3Qt5
'''

import argparse
import sys
import os
import logging
# https://www.xlwings.org/
# pip install xlwings==0.11.4
import xlwings as xw
from shutil import copyfile
import datetime
import re
# https://docs.google.com/document/d/1JGFNWkyTebbTH9bxOrFfOsv6xiSxR0N-QSIRf1HjRis/edit


# Print iterations progress
def printProgress (iteration, total, prefix = '', suffix = '', decimals = 1, barLength = 100, fill = '='):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(barLength * iteration // total)
    bar = fill * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percent, '%', suffix)),
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


'''
req 1: Parse through one of the Excel workbook files in the "Master" directory and create a subfolder for each tab(worksheet) in 
the file that is named with a number/ID. (You should now have subfolders named "1", "2", "3", etc)
'''    
    
def phase1_create_folder_structure(master_folder):
    
    print("Start folders creation...")
    print("Root directory: " + master_folder)
    
    logging.info("Root directory: " + master_folder)
    error = False
    
    files = os.listdir( master_folder )
    
    for file_name in files:
        if ".xlsm" not in file_name:
            continue
        try:
            wb = xw.Book(master_folder + file_name )            
            for i in range(1, wb.sheets.count):
                sht = wb.sheets[i]
                sheet_name = sht.name
                logging.info('%s - Sheet Name: %s' % (file_name, sheet_name) )
                try: 
                    int(sheet_name)
                except ValueError:
                    continue
                if not os.path.exists(master_folder  + sheet_name):
                    os.makedirs(master_folder  + sheet_name)
                
                printProgress(i, wb.sheets.count-1, prefix = '', suffix = 'Complete', barLength = 50)
            wb.close()
            
            print("Folders created succefully")
            logging.info("Folders created succefully")
        except Exception as e:
            print("Folders created with errors")
            logging.info( str(e) )
            print(str(e))
            error = True
        finally:
            break
    
    return error

def move_excel(master_folder, app, sht, cnt, file_code):
    #copy to a new workbook
    
    if app is not None:
        sht.api.Copy()
        wb = app.books.active
        wb.save(master_folder + str(cnt)+ os.sep + file_code + ".xlsx")
        wb.close()
        return
    

'''
Walk through each workbook in the Masterdirectory, export each tab(worksheet) into its own file, and move them to the appropriate sub directory, then rename the file to the value in cell "c1". 
For instance in all workbook files each tab(worksheet) named "1" should be exported and saved as separate files, placed in the folder named "1" and their filename should be set to the value in their cell "C1" (i.e. 1234.xlsx)
Only export the tabs(worksheets) that are named with a number (i.e. 1, 2, 3, etc).  Don't export any other existing tabs. the tabs
'''

def phase1_export_worksheets_2_folder(app, master_folder):
    
    print("Start exporting sheets to specific folders")
    logging.info("Start exporting sheets to specific folders");
    files = os.listdir( master_folder )

    for file_name in files:
        if ".xlsm" not in file_name:
            continue

        try:                                           
            wb = xw.Book(master_folder + file_name )
            for i in range(1, wb.sheets.count):
                sht = wb.sheets[i]
                sheet_name = sht.name
                logging.info('%s - Sheet Name: %s' % (file_name, sheet_name) )
                try:
                    int(sheet_name)
                except ValueError:
                    continue
                file_code = file_name[:-len(".xlsm")]
                move_excel(master_folder, app, sht, i-1, file_code)
                printProgress(i, wb.sheets.count-1, prefix = '%s:'% file_name, suffix = 'Complete', barLength = 50)
            wb.close()
            
        except Exception as e:
            print(str(e))
            break
    
    logging.info("Operation completed!");    
    print("Operation completed!")
    
def phase1(app, master_folder):
    print("Start Phase 1 - folders creation and export...")
    print("Root directory: " + master_folder)    
    logging.info("Root directory: " + master_folder)
        
    phase1_create_folder_structure(master_folder)
    phase1_export_worksheets_2_folder(app, master_folder)
    
    logging.info("Start Phase 1 - done...")
    print("\nStart Phase 1 - done...\n")
    

def phase2_process_folder(app, master_folder, excel_files):

    logging.info("phase2_process_folder start... ");
    for file_name in excel_files:
        if ".xlsm" not in file_name and ".xlsx" not in file_name:
            continue
        wb = xw.Book(master_folder +os.sep + file_name )
        sht = wb.sheets[0]
        sht.range('B1').value = ""

        for i in range(3):
            sht.range("A:A").api.Insert( xw.constants.InsertShiftDirection.xlShiftToRight )    # insert blank column pushing right previous data

        for i in range(5):
            sht.range("A1:Z1").api.Insert( xw.constants.InsertShiftDirection.xlShiftDown )    # insert blank row pushing down previous data

        m = re.finditer("(\w+)\.xl\w+$", file_name )
        for match in m:
            sht.range('A1').value = match.group(1)

        sht.range('A2').value = "a."
        sht.range('A3').value = "b."
        sht.range('A4').value = "c."
        sht.range('A5').value = "d."
        sht.range('A6').value = "e."
        sht.range('A7').value = "f."
        sht.range('A8').value = "g."
        sht.range('A9').value = "h."

        sht.range('B1').formula = '=IF(SUM(B2:B3)<=D8,SUM(B2:B3),"Exceeds Max Points")'
        
        wb.save()
        wb.close()
        
        logging.info("phase2_process_folder done ");
        
def phase2_merge_sheets(app, master_folder, cnt):
    
    logging.info("phase2_merge_sheets start [%s]... " % cnt);
    try:
        os.remove(master_folder +os.sep + "%s.xlsm" % cnt)
    except:
        pass
    try:
        os.remove(master_folder +os.sep + "merge_sheets.xlsm")
    except:
        pass
    try:
        os.remove(master_folder +os.sep + "GIS.xlsx")
    except:
        pass
    
    copyfile("./helper/merge_sheets.xlsm", master_folder +os.sep + "merge_sheets.xlsm")
    wb = xw.Book(master_folder +os.sep + "merge_sheets.xlsm")
    wb.macro("mergeSheets")( os.path.abspath(master_folder)+os.sep )
    copyfile("./GIS Template/GIS.xlsx", master_folder +os.sep + "GIS.xlsx")
    wb.macro("mergeGIS")( os.path.abspath(master_folder)+os.sep )
    wb.save(master_folder +os.sep + "merge_sheets.xlsm")    
    wb.close()
    
    os.remove(master_folder+os.sep+ "GIS.xlsx")
    os.rename(master_folder+os.sep+ "merge_sheets.xlsm", master_folder+os.sep+ "%s.xlsm" % cnt)
    logging.info("phase2_merge_sheets done [%s]... " % cnt);
    
        
def phase2(app, master_folder):
    print("Start Phase 2 - folders analisys...")
    print("Root directory: " + master_folder)    
    logging.info("Root directory: " + master_folder)

    
    for i in range(1, 26):        
        
        logging.info("Process directory: " + master_folder +str(i))
        files = os.listdir( master_folder + str(i) )
        
        phase2_process_folder(app,master_folder + str(i), files)    
        phase2_merge_sheets(app,master_folder + str(i), i)
        
        for file_name in files:
            if ".xlsm" not in file_name and ".xlsx" not in file_name:
                continue
            try:
                os.remove(master_folder + str(i) + os.sep + file_name)
            except Exception as e:
                print(str(e))
    
        printProgress(i, 25,  prefix = '[%s]:'% i, suffix = 'Complete', barLength = 50)

    logging.info("Start Phase 2 - done...")
    print("\nStart Phase 2 - done...\n")

def process_phase3_excel(app, excel_file):
        wb = xw.Book( excel_file )
        sht = wb.sheets[0]

        formula = "=IF(OR(AND(F?=-1,COUNTBLANK(G?:M?)=7),COUNTBLANK(F?:M?)=8),-1,SUM(IF(F?>0,F?,0)+IF(G?>0,G?,0)+IF(H?>0,H?,0)+IF(I?>0,I?,0)+IF(J?>0,J?,0)+IF(K?>0,K?,0)+IF(L?>0,L?,0)+IF(M?>0,M?,0)))"
        for i in range (2, 6):            
            sht.range('A' + str(i) ).formula = formula.replace("?",str(i))
        
        sht.range('B1').value = ""


        sht.range('A2').value = "a."
        sht.range('A3').value = "b."
        sht.range('A4').value = "c."
        sht.range('A5').value = "d."
        sht.range('A6').value = "e."
        sht.range('A7').value = "f."
        sht.range('A8').value = "g."
        sht.range('A9').value = "h."


        
        wb.save()
        wb.close()


def phase3(app,master_folder):
    print("Start Phase 3 - folders analisys...")
    print("Root directory: " + master_folder)    
    logging.info("Root directory: " + master_folder)

    
    for i in range(1, 26):
        logging.info("Process directory: " + master_folder +str(i))
        process_phase3_excel(app, master_folder+os.sep+ "%s.xlsm" % i)
        printProgress(i, 25,  prefix = "%s.xlsm" % i, suffix = 'Complete', barLength = 50)

    logging.info("Start Phase 3 - done...")
    print("\nStart Phase 3 - done...\n")

def final_task(app, master_folder, password):
    print("Start Final Task - folders analisys...")
    print("Root directory: " + master_folder)    
    logging.info("Root directory: " + master_folder)

    
    for i in range(1, 26):
        logging.info("Process directory: " + master_folder +str(i))
        
        if password is not None:
            
            file_path = master_folder+str(i) + os.sep+ "%s.xlsm" % i
            wb = xw.Book( file_path )
            #wb.macro("lock_sheet")( "Foglio1", password )
            wb.macro("lock_workbook")( password )
            #wb.macro("unlock_sheet")( "Foglio1", "ale" )
            wb.save( file_path )    
            wb.close()
        printProgress(i, 25, prefix = '', suffix = 'Complete', barLength = 50)

    logging.info("Start Final Task - done...")
    print("\nStart Final Task - done...\n")
    
if __name__ == '__main__':
            
    parser = argparse.ArgumentParser(description='Excel Manipulator ver. 1.0')
    parser.add_argument('-directory', dest='directory',
                        default = './Master/',
                        help='Set the Master  folder to parse.')
    parser.add_argument('-password', dest='password',
                        default = None,
                        help='Set Password to lock the workbook')

    
    args = parser.parse_args()
    master_directory_path = args.directory
    password = args.password

    logging.basicConfig(filename='./run_%s.log' % datetime.datetime.now().strftime('%Y%m%d_%H%M'),level=logging.INFO)
    logging.info('Start Engine')
    logging.info('directory: %s' % master_directory_path)

    if master_directory_path[-1] != '/' and master_directory_path[-1] != '\\':   # windows compatibility
        master_directory_path = master_directory_path + os.sep

    app = None
    try:
        app = xw.App(visible=False)
        app.screen_updating=False
    except Exception as e:
        logging.error(str(e) )
        print(str(e))
        exit(-1)

    try:
        
        #wb = xw.Book(master_directory_path + "test.xlsm")
        #wb.macro("lock_sheet")( "Foglio1", "ale" )
        #wb.macro("unlock_sheet")( "Foglio1", "ale" )
        #wb.save(master_directory_path + "test.xlsm")    
        #wb.close()
        
        phase1(app, master_directory_path)
        
        phase2(app, master_directory_path)
        
        #phase3(app, master_directory_path)
        
        final_task(app, master_directory_path, password)
    except Exception as e:
        print(str(e))
        logging.error(str(e))
    finally:
        if app is not None:
            app.screen_updating=True
            app.kill()            

    
    logging.info('done!!!')
    print("Done!")
